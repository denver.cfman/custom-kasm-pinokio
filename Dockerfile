#FROM kasmweb/core-ubuntu-focal:1.14.0
#FROM kasmweb/core-ubuntu-jammy:1.14.0
FROM kasmweb/ubuntu-focal-desktop:1.14.0
#FROM kasmweb/ubuntu-jammy-dind:1.14.0
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########

RUN apt-get update && apt-get upgrade -y && apt-get install xdg-utils git curl -y

#RUN REL=$(curl -s -I https://github.com/pinokiocomputer/pinokio/releases/latest |grep -i location |awk -F "/" '{print $NF}') && echo $REL
RUN REL=1.0.16 curl -LO https://github.com/pinokiocomputer/pinokio/releases/download/1.0.16/Pinokio_1.0.16_amd64.deb || true
RUN dpkg -i Pinokio_1.0.16_amd64.deb

COPY <<EOF /home/kasm-user/Desktop/Pinokio.desktop
[Desktop Entry]
Version=1.0
Type=Application
Name=Pinokio
Comment=
Exec=pinokio --no-sandbox
Icon=
Path=~/
Terminal=true
StartupNotify=false
EOF

######### End Customizations ###########

RUN chown -Rf 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME

RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000
